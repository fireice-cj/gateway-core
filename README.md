## Genral information <br />

this api is made using python 3.9<br />
<br />

## Folder structure<br />

/ <br />
-- lib/ <br />
---- do/ <br />
---- firewall/ <br />
---- ips <br />
-- routes/ <br />
-- scripts/ <br />
<br />

- **lib folder** - contains all the functions and classes for the endpoints on the routes folder.
  - **do folder** - digital ocean plugin endpoint functions
  - **firewall folder** - firewall endpoint functions
  - **ips folder** - ips endpoint functions <br />
- **root folder** - contains the main.py file which handles the main fastapi routing. <br />
- **routes folder** - contains all endpoints classes for the API. <br />
- **scripts folder** - contains the scripts to run on the terminal for the endpoint <br />
  <br />

**The server python verion is now upgraded from 3.8.6 to 3.9.10** <br />
**If working directly on the server activate `pipenv` by typing `pipenv shell`.**
