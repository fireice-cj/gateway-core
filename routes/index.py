from fastapi import APIRouter, status

router = APIRouter(
    tags=['Index']
)


@router.get('/', status_code=status.HTTP_200_OK)
def root():
    return {"message": "FireIce Gateway Core API Service"}

